/**
 * View Models used by Spring MVC REST controllers.
 */
package com.wasle.web.rest.vm;
