package com.wasle.domain;



import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.*;

/**
 * A Student.
 */
@Entity
@Table(name = "student")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "matrikelnummer")
    private Long matrikelnummer;

    @NotNull
    @Column(name = "vorname", nullable = false)
    private String vorname;

    @NotNull
    @Column(name = "nachname", nullable = false)
    private String nachname;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "postleitzahl")
    private Integer postleitzahl;

    @Column(name = "ort")
    private String ort;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMatrikelnummer() {
        Long id = this.getId();
		int idint;
		if (id != null) {
			idint = id.intValue();
		} else idint = 0;
		int year = Calendar.getInstance().get(Calendar.YEAR)%100;
		int number = (((year*100)+30)*1000)+idint;
		
		return new Long(number);
		
        
    }

    public Student matrikelnummer(Long matrikelnummer) {
        Long id = this.getId();
		int idint;
		if (id != null) {
			idint = id.intValue();
		} else idint = 0;
		int year = Calendar.getInstance().get(Calendar.YEAR)%100;
		int number = (((year*100)+30)*1000)+idint;
		
		this.matrikelnummer = new Long(number);
        return this;
    }

    public void setMatrikelnummer(Long matrikelnummer) {
		Long id = this.getId();
		int idint;
		if (id != null) {
			idint = id.intValue();
		} else idint = 0;
		int year = Calendar.getInstance().get(Calendar.YEAR)%100;
		int number = (((year*100)+30)*1000)+idint;
		
		this.matrikelnummer = new Long(number);
    }

    public String getVorname() {
        return vorname;
    }

    public Student vorname(String vorname) {
        this.vorname = vorname;
        return this;
    }

    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public Student nachname(String nachname) {
        this.nachname = nachname;
        return this;
    }

    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    public String getAdresse() {
        return adresse;
    }

    public Student adresse(String adresse) {
        this.adresse = adresse;
        return this;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Integer getPostleitzahl() {
        return postleitzahl;
    }

    public Student postleitzahl(Integer postleitzahl) {
        this.postleitzahl = postleitzahl;
        return this;
    }

    public void setPostleitzahl(Integer postleitzahl) {
        this.postleitzahl = postleitzahl;
    }

    public String getOrt() {
        return ort;
    }

    public Student ort(String ort) {
        this.ort = ort;
        return this;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Student student = (Student) o;
        if (student.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), student.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Student{" +
            "id=" + getId() +
            ", matrikelnummer=" + getMatrikelnummer() +
            ", vorname='" + getVorname() + "'" +
            ", nachname='" + getNachname() + "'" +
            ", adresse='" + getAdresse() + "'" +
            ", postleitzahl=" + getPostleitzahl() +
            ", ort='" + getOrt() + "'" +
            "}";
    }
	
}
