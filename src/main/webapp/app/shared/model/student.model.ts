export interface IStudent {
    id?: number;
    matrikelnummer?: number;
    vorname?: string;
    nachname?: string;
    adresse?: string;
    postleitzahl?: number;
    ort?: string;
}

export class Student implements IStudent {
    constructor(
        public id?: number,
        public matrikelnummer?: number,
        public vorname?: string,
        public nachname?: string,
        public adresse?: string,
        public postleitzahl?: number,
        public ort?: string
    ) {}
}
